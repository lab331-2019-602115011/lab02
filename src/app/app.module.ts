import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from 'src/service/student-service';
import { StudentDataImplService } from 'src/service/student-data-impl.service';
import { NewStudentDataImplService } from 'src/service/new-student-data-impl.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [{
    provide: StudentService, useClass: StudentDataImplService
  },
  { provide: StudentService, useClass: NewStudentDataImplService }],
  bootstrap: [AppComponent]
})
export class AppModule { }
