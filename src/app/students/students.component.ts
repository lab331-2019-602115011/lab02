import { Component, OnInit } from '@angular/core';
import { Student } from 'src/entity/student';
import { StudentDataImplService } from 'src/service/student-data-impl.service';
import { StudentService } from 'src/service/student-service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students: Student[];

  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getStudents().subscribe(
      students => {
      this.students = students;
      });
  }

  averageGpa(): number {
    let sum = 0;
    for (let student of this.students) {
      sum += student.gpa;
    }
    return sum / this.students.length;
  }

}
