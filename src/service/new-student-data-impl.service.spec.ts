import { TestBed, inject } from '@angular/core/testing';

import { NewStudentDataImplService } from './new-student-data-impl.service';

describe('NewStudentDataImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewStudentDataImplService]
    });
  });

  it('should be created', inject([NewStudentDataImplService], (service: NewStudentDataImplService) => {
    expect(service).toBeTruthy();
  }));
});
