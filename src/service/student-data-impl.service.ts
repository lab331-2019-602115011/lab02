import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Student } from 'src/entity/student';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class StudentDataImplService extends StudentService {
  getStudents(): Observable<Student[]> {
    return of(this.students);
  }

  students: Student[] = [{
    'id': 1,
    'studentId': '562110507',
    'name': 'Prayuth',
    'surname': 'Tu',
    'gpa': 4.00
  }, {
    'id': 2,
    'studentId': '562110509',
    'name': 'Pu',
    'surname': 'Priya',
    'gpa': 2.12

  }];

  constructor() {
    super();
  }
}
