import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Student } from 'src/entity/student';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewStudentDataImplService extends StudentService{
  getStudents(): Observable<Student[]> {
    return of(this.students);
  }

  students: Student[] = [{
    'id': 3,
    'studentId': '602115011',
    'name': 'Thipnirun',
    'surname': 'Neamsuntea',
    'gpa': 3.17
  }, {
    'id': 4,
    'studentId': '592110509',
    'name': 'Dong wook',
    'surname': 'Lee',
    'gpa': 3.58

  }];
  constructor() {
    super();
  }
  
}
